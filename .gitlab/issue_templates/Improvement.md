Summary:

(Summary of your improvement)

Improvement details:

(Please provide a brief description of the improvement, and what parts of the website it affects)

Urgency:

(Is this improvement time sensitive?)

(Eg, An AGM feature should be ready by AGM)

\label ~Improvement